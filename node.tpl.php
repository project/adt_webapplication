  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture) {
      print $picture;
    }?>
    <?php if ($page == 0) { ?><h1 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h1><?php }; ?>
    <?php if ($submitted) { ?><span class="submitted"><?php print $submitted?></span><?php }; ?>		
    <div class="content"><?php print $content?></div>
    <?php if ($terms) { ?><div class="taxonomy"><?php print $terms?></div><?php }; ?>		
    <?php if ($links) { ?><div class="links"><?php print $links?></div><?php }; ?>
  </div>