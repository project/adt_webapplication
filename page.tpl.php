<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>	
	<?php
	$mainwidth = theme_get_setting('cto_main_width');
	if (($sidebar_left) && ($sidebar_right)) {
  	$sidebarwidth = (95 - $mainwidth) / 2; //100 -1%ie roundoff error minus 2+2 sidebar margins
	} else {
  	$sidebarwidth = 97 - $mainwidth;	//100 -1ie% roundoff error minus 2 sidebar
	}
	?>
	<style type="text/css">
	.container {width:<?php print theme_get_setting('cto_layout_width');print theme_get_setting('cto_fixedfluid'); ?>;}
	#main {width:<?php print $mainwidth; ?>%;}
	.sidebar {width:<?php print $sidebarwidth; ?>%;}
	</style>

  <?php if (theme_get_setting('cto_iepngfix')) { ?>
  <!--[if lte IE 6]>
    <script type="text/javascript"> 
        $(document).ready(function(){ 
            $(document).pngFix(); 
        }); 
    </script> 
  <![endif]-->
  <?php } ?>	
	
	
</head>
<body <?php if (arg(0) == 'admin') { print 'class="adminpage"'; } ?>>

	<div id="header">
	<div class="container">
		<?php if ($logo) { ?><a id="logo" href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
		<?php if (isset($primary_links)) { print theme('menu_links', $primary_links); } ?>		
		<?php if ($site_name) { ?><h1 id="logo-text"><a href="<?php print $base_path ?>" title=""><?php print $site_name ?></a></h1><?php } ?>
		<?php if ($site_slogan) { ?><h2 id="slogan"><?php print $site_slogan ?></h2><?php } ?>
	</div>
	</div>

	<?php if (theme_get_setting('cto_subheader') != 1) { ?>
  	<div id="teaser">
		<div class="container">
  		<?php if ($header) { print $header; } ?>
  	</div>
		</div>
	<?php } ?>

		<?php if (theme_get_setting('cto_navbar') != 1) { ?>
  		<div id="bar">
			<div class="container">
  		<?php if (isset($secondary_links)) { ;print theme('menu_links', $secondary_links); } ?>
  		<?php if ($navbar) { print $navbar; } ?>
  		</div>
			</div>
		<?php } ?>

	<div class="container">			
		<?php if ($sidebar_left) { ?><div class="sidebar left"><?php print $sidebar_left ?></div><?php } ?>
		<div id="main">
		<?php if ($mission) { ?><h3 id="mission"><?php print $mission ?></h3><?php } ?>
				 
				<?php print $breadcrumb ?>
        <?php if ($title) { ?><h1 class="title"><?php print $title ?></h1><?php } ?>
        <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>		
										
		</div><!-- end main -->		
		<?php if ($sidebar_right) { ?><div class="sidebar right" ><?php print $sidebar_right ?></div><?php } ?>		

		
	<div id="schoen">
		 <?php if ($footer_message) { print $footer_message; } 
		 // leaving the credits below is not obligatory as per the license but it shows your appreciation and support :) ?>
		<p>Design - <a href="http://www.solucija.com/" title="Information Architecture and Web Design">Luka Cvrk</a>. Drupal Port by ADT <a href="http://www.alldrupalthemes.com">Drupal Themes</a>.</p>
	</div>
	
	</div><!-- end container -->
	
	<div style="visibility:hidden;"><?php print $closure;?>sfy39587f11</div>		
	 
</body>
</html>