<?php
function adt_webapplication_regions() {
  return array(
    'right' => t('right sidebar'),
    'left' => t('left sidebar'),		
    'content' => t('content'),
    'header' => t('header'),
    'navbar' => t('navbar'),				
    'footer' => t('footer'),
  );
}

/*
* Initialize theme settings
*/
if (is_null(theme_get_setting('cto_fixedfluid'))) {  // <-- if settings have never ben set
  global $theme_key;
  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the theme-settings.php file.
   */
  $defaults = array(
    'cto_layout_width' => 900,
		'cto_fixedfluid' => 'px',
		'cto_main_width' => '60',	
    'cto_subheader' => 0,
    'cto_navbar' => 0,		
    'cto_font' => '',
    'cto_iepngfix' => 1,												
  );

  // Save theme settings with the defaults
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, theme_get_settings($theme_key))
  );
  // Force refresh of Drupal internals
  theme_get_setting('', TRUE);
}

if (theme_get_setting('cto_dropdown')) {
   drupal_add_js(drupal_get_path('theme', 'adt_webapplication') . '/js/ddown.js', 'theme');
}
if (theme_get_setting('cto_iepngfix')) {
   drupal_add_js(drupal_get_path('theme', 'adt_webapplication') . '/js/jquery.pngFix.pack.js', 'theme');
}
