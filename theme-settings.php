<?php
// An example themes/garland/theme-settings.php file.

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function phptemplate_settings($saved_settings) {
  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the template.php file.
   */
  $defaults = array(
		'cto_fixedfluid' => 'px',	
    'cto_layout_width' => 900,
		'cto_main_width' => '60',
    'cto_subheader' => 0,
    'cto_navbar' => 0,					
    'cto_font' => '',
    'cto_iepngfix' => 1,									
  );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

	  // Create the form widgets using Forms API
  $form['layout'] = array(
    '#title' => t('Layout Options'),
		'#type' => 'fieldset',
	);	
	
	$form['layout']['cto_fixedfluid'] = array(
    '#type' => 'select',
    '#title' => t('Fixed or Fluid width'),
    '#default_value' => $settings['cto_fixedfluid'],
    '#options' => array(
     'px' => t('Fixed width (in pixels)'),
     '%' => t('Fluid width (as percentage of viewport width)'),		 
    ),
  );
	
  $form['layout']['cto_layout_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Layout Width'),
    '#default_value' => $settings['cto_layout_width'],
    '#size' => 4,
    '#maxlength' => 4,
		'#description' => t('If you chose "relative" in the previous option fill in the percentage, do not append % sign. If you chose fixed width, fill in the number of pixels you want to layout to span.')
  );
	
  $form['layout']['cto_main_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Main Width'),
    '#default_value' => $settings['cto_main_width'],
    '#size' => 3,
    '#maxlength' => 3,
		'#description' => t('Main content area width as percentage of layout width. The sidebar(s) will automatically assume the leftover width. For example, if you fill in 60, your content area will take up 60% of the available width, and if you have one sidebar (2 sidebars) it will take up approximately 40% (20%) of the available width.'),
		'#validate' => array('is_percentage_between' => array('cto_main_width', 0,100)), 
  );
	
  $form['layout']['cto_subheader'] = array(
    '#type' => 'checkbox',
    '#title' => t('Omit Subheader'),
    '#default_value' => $settings['cto_subheader'],
  );			

  $form['layout']['cto_navbar'] = array(
    '#type' => 'checkbox',
    '#title' => t('Omit Nav bar (contains secondary links menu)'),
    '#default_value' => $settings['cto_navbar'],
  );
	
  $form['cto_font'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#default_value' => $settings['cto_font'],
    '#options' => array(
     'Arial, Verdana, sans-serif' => t('Arial, Verdana, sans-serif (Default)'),
     'Verdana, Arial, sans-serif' => t('Verdana, Arial, sans-serif'),		 
     'Courier New, Courier, monospace' => t('Courier New, Courier, monospace'),
     'Tahoma, Verdana, Arial, Helvetica, sans-serif' => t('Tahoma, Verdana, Arial, Helvetica, sans-serif'),
     'Trebucher MS,Trebuchet, Verdana, Arial, Helvetica, sans-serif' => t('Trebucher MS,Trebuchet, Verdana, Arial, Helvetica, sans-serif'),		 		 
     'Times New Roman, Times, serif' => t('Times New Roman, Times, serif'),
     'Georgia, Times New Roman, Times, serif' => t('Georgia, Times New Roman, Times, serif'),
    ),
  );
	
  $form['cto_iepngfix'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use IE PNG Fix'),
    '#default_value' => $settings['cto_iepngfix'],
  );
	
  function is_percentage_between ($formelement, $fieldname, $min=NULL, $max=NULL) {
  $thevalue = $formelement['#value'];
      if (is_numeric($thevalue)) {
      $thevalue = $thevalue + 0;
      } else {
        form_set_error($fieldname, t('Percentage must be an integer. Please omit % sign.'));
      }
  if (!is_int($thevalue)) {
        form_set_error($fieldname, t('Percentage must be an integer. Please omit % sign.'));
  } else {
    if (isset($min) && ($thevalue < $min)) {
      form_set_error($fieldname, t("Percentage must be at least $min."));
    }
    if (isset($max) && ($thevalue > $max)) {
      form_set_error($fieldname, t("Percentage must be no higher than $max."));
    }
  }
  }
  function cto_integer ($formelement, $fieldname) {
    $thevalue = $formelement['#value'];
        if (is_numeric($thevalue)) {
        $thevalue = $thevalue + 0;
        } else {
          form_set_error($fieldname, t('width must be an integer.'));
        }
    if (!is_int($thevalue)) {
          form_set_error($fieldname, t('width must be an integer.'));
    }
  }								

  // Return the additional form widgets
  return $form;
}
?>